var express = require('express');
var router = express.Router();
var validate = require('express-validation');
import user from '../controllers/user.js';
import admin from '../controllers/admin.js';
import comman from '../controllers/comman.js';
const fs = require('fs');

/* auth routes for admin*/
router.get('/all-kpis', comman.listAllKpis);
//comman
router.get('/v1/auth/all-kpis', comman.listAllKpis);
//users table
router.get('/v1/auth/list-user/:skip/:limit', user.listUsers);
router.post('/v1/auth/edit-admin-user', user.editAdminUser);
router.post('/v1/auth/delete-user/:userId/:skip/:limit', user.deleteUser);
router.post('/v1/auth/change-user-status', user.changeStatus);
router.post('/v1/auth/filter-user/:skip/:limit', user.filterUser);
//admin table
router.post('/v1/auth/change-admin-status', admin.changeStatus);

/* unAuth routes admin*/
//admin table
router.post('/v1/create-admin', admin.registerAdmin);
router.post('/v1/admin-login', admin.adminLogin);

/* auth routes for application*/
//users table
router.post('/v1/auth/edit-user', user.editUser);
router.get('/v1/auth/get-user-detail', user.getUserDetail);

/* unAuth routes application*/
//users table
router.post('/v1/create-user', user.registerUser);
router.post('/v1/login', user.login);
router.post('/v1/generate-otp', user.generateOtp);
router.post('/v1/verify-otp', user.verifyOtp);

/* render images for user */
router.get('/user/images/:name', (req, res) => {
	const fileName = req.params.name;
	console.log('fileName printed here', fileName);
	try {
		let file = fs.readFileSync(`./public/user/${fileName}`);
		res.writeHead(200, { 'Content-Type': 'image/jpg' });
		res.end(file, 'binary');
	} catch (err) {
		res.status(400).send(err.message);
	}
});

module.exports = router;
