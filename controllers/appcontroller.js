class AppController {
  constructor() {}

  getDate() {
    var d = new Date();
    return (
      [d.getFullYear(), d.getMonth() + 1, d.getDate()].join(":") +
      " " +
      [d.getHours(), d.getMinutes(), d.getSeconds()].join(":")
    );
  }

  getOtp() {
    // Declare a digits variable
    // which stores all digits
    var digits = "0123456789";
    let OTP = "";
    for (let i = 0; i < 4; i++) {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

  checkOtpExpire(generated_at) {
    let todayTime = moment();
    let minutes = moment
      .duration(todayTime.diff(moment(generated_at)))
      .asMinutes();

    console.log(
      "!!!!!!!!otpData minutes printed here",
      todayTime,
      minutes,
      generated_at
    );

    if (minutes <= 5) {
      return false;
    } else {
      return true;
    }
  }
}

export default AppController;
