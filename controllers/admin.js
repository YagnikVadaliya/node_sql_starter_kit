import validator from "validator";
import AWS from "aws-sdk";
import raw from "objection";
import AppController from "./appcontroller";
import constants from "../configuration/constants";
import bcrypt from "bcryptjs";
import token from "../configuration/secret";
var jwt = require("jsonwebtoken");
const models = require("../models");

class Admin extends AppController {
  constructor() {
    super();
  }

  /* Admins functions */

  /**
   * @api {post} /v1/auth/change-status (change status of admin)
   * @apiName changeStatus
   * @apiGroup Admin
   *
   * @apiParam {String} adminId adminId .
   * @apiParam {Boolean} flag flag
   *
   * @apiSuccess {Object} Admin.
   */
  async changeStatus(req, res) {
    console.log("changeStatus function start");
    try {
      if (!req.body.adminId || !req.body.flag) {
        throw "Please provide all Data";
      }

      let adminObject = await models.admins.update(
        {
          is_active: flag,
        },
        {
          where: {
            id: req.body.adminId,
          },
        }
      );

      res.json({
        status: constants.success_code,
        message: "Admin status changed Successfully",
        data: adminObject,
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/create-admin (admin register)
   * @apiName registerAdmin
   * @apiGroup Admin
   *
   * @apiParam {String} first_name first_name .
   * @apiParam {String} email email
   * @apiParam {String} password password
   * @apiParam {String} last_name last_name (Optional) .
   * @apiParam {String} phone phone (Optional).
   *
   * @apiSuccess {Object} Admin.
   */
  async registerAdmin(req, res) {
    console.log("registerAdmin function start");
    try {
      if (!req.body.first_name || !req.body.email || !req.body.password) {
        throw "Please provide all Data";
      }

      let secret = token();
      var hashedPassword = bcrypt.hashSync(req.body.password, 8);
      var uniqueId = "_" + Math.random().toString(36).substr(2, 9);

      let createObject = {
        id: uniqueId,
        first_name: req.body.first_name,
        email: req.body.email,
        password: hashedPassword,
        is_active: true,
        role: "Admin",
        _deleted: false,
      };

      if (req.body.last_name) {
        createObject.last_name = req.body.last_name;
      }

      if (req.body.phone) {
        createObject.phone = req.body.phone;
      }

      console.log("before create!!!!!!!!!!!!!!", createObject);
      let admin = await models.admins.create(createObject);
      console.log("admin printed here after creation", admin, admin.id);

      let responseObject = {
        id: admin.id,
        first_name: admin.first_name,
        last_name: admin.last_name,
        email: admin.email,
        phone: admin.phone,
        is_active: admin.is_active,
        createdAt: admin.createdAt,
      };
      responseObject.authToken = jwt.sign(
        { id: admin.id, role: "Admin" },
        secret,
        {
          expiresIn: 86400, // expires in 24 hours
        }
      );

      console.log("responseObject printed here", responseObject);

      res.json({
        status: constants.success_code,
        message: "successfully created",
        data: responseObject,
        authToken: responseObject.authToken,
      });

      return;
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/admin-login (admin login)
   * @apiName adminLogin
   * @apiGroup Admin
   *
   * @apiParam {String} email email
   * @apiParam {String} password password
   *
   * @apiSuccess {Object} Admin.
   */
  async adminLogin(req, res) {
    console.log("login function start");
    try {
      if (!req.body.email || !req.body.password) {
        throw "Please provide all Data";
      }

      let admin = await models.admins.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (!admin) {
        throw "Invalid Username";
      }

      var isEqual = bcrypt.compareSync(req.body.password, admin.password);
      console.log("!!!!!!!!!!after compare", isEqual);

      if (!isEqual) {
        throw "Invalid Password";
      }

      let responseObject = {
        id: admin.id,
        first_name: admin.first_name,
        last_name: admin.last_name,
        email: admin.email,
        phone: admin.phone,
        is_active: admin.is_active,
        createdAt: admin.createdAt,
      };

      let secret = token();

      responseObject.authToken = jwt.sign(
        { id: admin.id, role: "Admin" },
        secret,
        {
          expiresIn: 86400, // expires in 24 hours
        }
      );

      res.json({
        status: constants.success_code,
        message: "successfully Logged in",
        data: responseObject,
        authToken: responseObject.authToken,
      });

      return;
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /*
    -------------------------
    comman functions
  */
}

export default new Admin();
