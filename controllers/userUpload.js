const express = require("express");
const multer = require("multer");
const path = require("path");
const fs = require("fs");

const UPLOAD_STOCK_DIR = "./public/stock";
const UPLOAD_DIR = "./public/stock";

class Uploader {
  constructor() {
    this.upload_dir = UPLOAD_DIR;
    this.storage = undefined;
  }

  uploader(field, res) {
    console.log("innn", field);
    return multer({
      storage: this.storage,
      fileFilter: (req, file, cb) => {
        console.log("i file filter", file);
        this.checkFileType(file, cb, res);
      },
    }).single(field);
  }

  checkFileType(file, cb, res) {
    console.log("file.orginalName", file.originalname, file.mimetype);
    // Allowed ext
    const filetypes = /image\/jpg|image\/jpeg|jpeg|jpg|png|xlsx|xls|application\/octet-stream|application\/vnd\.ms-excel|application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|jfif|aac|mp4|flv|avi|wav|m4a|wmv|mkv|ogg|mov|webm|pdf|3gp|audio\/mpeg|audio\/x-mpeg-3|audio\/wav|audio\/x-wav|audio\/ogg|audio\/x-ogg|audio\/m4a|audio\/aac|acc|audio\/acc|mp3/;
    // const filetypes = /image\/jpg|image\/jpeg|jpeg|jpg|png/;
    // Check ext
    const extname = filetypes.test(
      path.extname(file.originalname).toLowerCase()
    );
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
      return cb(null, true);
    } else {
      cb("File type not allowed");
    }
  }

  handler() {
    const app = express.Router();

    app.post("/:folder/:field", (req, res) => {
      console.log(
        "Enter Upload",
        req.params.folder,
        req.params.field,
        req.file,
        req.uri
      );

      if (req.params.folder == "stock" && req.params.field == "excel") {
        this.upload_dir = UPLOAD_STOCK_DIR + "/excel";
      } else {
        this.upload_dir = UPLOAD_DIR;
      }

      console.log("!!!!!!!!!!!!!!upload dir", this.upload_dir);
      this.storage = multer.diskStorage({
        destination: this.upload_dir,
        filename: function (req, file, cb) {
          cb(
            null,
            file.fieldname + "_" + Date.now() + path.extname(file.originalname)
          );
        },
      });

      const upload = this.uploader(req.params.field || "file", res);

      upload(req, res, (err) => {
        console.log("in upload function", req.file);
        if (err) {
          console.log("!!!!!!!!!!!!!!!!!!first err", err);
          res.send({
            code: "ERROR_IN_UPLOAD",
            msg: err,
          });
        } else {
          if (req.file == undefined) {
            console.log("Error: No File Selected!");
            res.send({
              code: "NO_FILE",
              msg: "No file selected",
            });
          } else {
            console.log("File Uploaded!");
            res.send({
              msg: "File uploaded successfully",
              code: "FILE_UPLOADED",
              file: `${req.file.filename}`,
            });
          }
        }
      });
    });

    return app;
  }
}

module.exports = new Uploader().handler();
