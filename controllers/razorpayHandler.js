const fetch = require("isomorphic-unfetch");
const express = require("express");
const request = require("request");

const router = express.Router();

// const WEB_URL = process.env.WEB_URL;
// const WEB_URL = "http://localhost:1192";
const WEB_URL = "http://62.171.146.121:1192";

// const RAZORPAY_KEY_ID = process.env.RAZORPAY_KEY_ID;
// const RAZORPAY_SECRET = process.env.RAZORPAY_SECRET;
const RAZORPAY_KEY_ID = "rzp_test_ArXaLpOWKT2TTq";
const RAZORPAY_SECRET = "kWdSzHtYvsHh29y4S5s9f5Fn";

function getOptions() {
  return {
    redirect: true,
    key: RAZORPAY_KEY_ID,
    image: "https://i.imgur.com/n5tjHFD.png",
    // callback_url: `${WEB_URL}/payment/rzp/handler/{orderId}?params=`
  };
}

const rzpRequestOld = {
  // amount: 100, // paisa
  // email: "yashshah9621@gmail.com",
  // contact: "8319496874",
  // order_id: pg_order_id, // razorpay create order's response id
  method: "card",
  "card[name]": "YASH SHAH",
  "card[number]": "5104015555555558",
  "card[cvv]": "123",
  "card[expiry_month]": "05",
  "card[expiry_year]": "2022",
};

router.get("/", async (req, res) => {
  try {
    console.log("BODY => ", req.query);
    const {
      transaction_id,
      user_id,
      amount,
      orderCode,
      name,
      email,
      contact,
      cardName,
      cardNumber,
      cardCvv,
      cardExpiryMonth,
      cardExpiryYear,
      method,
      bank,
      wallet,
      emiDuration,
    } = req.query;

    console.log("get request query ", req.query);
    const amountInPaise = amount * 100;

    console.log(
      "!!!!!!!!!!!!before create razorpayOrder",
      amountInPaise,
      orderCode,
      email
    );

    const razorpayOrder = await createRazorpayOrder({
      amount: amountInPaise,
      orderCode: orderCode,
    });

    console.log("------------razorpayOrder ", razorpayOrder);

    const options = getOptions();
    options.amount = amountInPaise;
    options.order_id = razorpayOrder.id;

    //dynamic

    // const rzpRequest = {
    //   method: "card"
    // };
    // if (method && method === "wallet") {
    //   rzpRequest.method = method;
    //   rzpRequest.wallet = wallet;
    // } else if (method && method === "netbanking") {
    //   rzpRequest.method = method;
    //   rzpRequest.bank = bank;
    // } else if (method && method === "emi") {
    //   rzpRequest.method = method;
    //   rzpRequest.emi_duration = emiDuration;
    //   rzpRequest["card[name]"] = name;
    //   rzpRequest["card[number]"] = cardNumber;
    //   rzpRequest["card[cvv]"] = cardCvv;
    //   rzpRequest["card[expiry_month]"] = cardExpiryMonth;
    //   rzpRequest["card[expiry_year]"] = cardExpiryYear;
    // } else {
    //   rzpRequest["card[name]"] = name;
    //   rzpRequest["card[number]"] = cardNumber;
    //   rzpRequest["card[cvv]"] = cardCvv;
    //   rzpRequest["card[expiry_month]"] = cardExpiryMonth;
    //   rzpRequest["card[expiry_year]"] = cardExpiryYear;
    // }

    //static

    let rzpRequest = { ...rzpRequestOld };

    rzpRequest.email = email;
    rzpRequest.contact = contact;
    rzpRequest.amount = amountInPaise;
    rzpRequest.order_id = razorpayOrder.id;

    options.callback_url = `${WEB_URL}/payment/rzp/handler?orderId=${
      razorpayOrder.id
    }&amount=${
      amountInPaise / 100
    }&orderCode=${orderCode}&user_id=${user_id}&transaction_id=${transaction_id}`;

    const payOptions = {
      options: JSON.stringify(options),
      rzpRequest: JSON.stringify(rzpRequest),
    };

    console.log(
      "!!!!!!!!!!!!!!!!rendering => payments/razorpay-payment",
      payOptions
    );

    res.render("payments/razorpay-payment", {
      payOptions,
    });
  } catch (err) {
    console.log("razorpayPayment.js error => ", err);
  }
});

router.post("/handler", (req, res) => {
  try {
    console.log("got post request");
    const { orderId, amount, orderCode, user_id, transaction_id } = req.query;
    const { error, razorpay_payment_id } = req.body;
    console.log("orderId and amount ", JSON.stringify(req.query));
    console.log("razorpay_payment_id ", razorpay_payment_id);

    let resBody = JSON.stringify({});
    try {
      resBody = JSON.stringify(req.body);
    } catch (err) {
      console.log("error JSON.stringify error ", err);
    }

    console.log("!!!!!!!!!!!!!resBody", resBody);

    const amountInPaise = amount * 100;
    validateTransaction(amountInPaise, razorpay_payment_id)
      .then((r) => {
        console.log("validateTransaction Res ", r);
        if (!r || r.error) {
          // redirect to fail page
          console.log(
            "!!!!!!!!!!!!!!!!!!validateTransaction error",
            r.error,
            r
          );

          //   updateTransaction(orderCode, {
          //     status: "UNSUCCESS",
          //     payment_gateway_order_id: razorpay_payment_id,
          //     transaction_response: resBody
          //   })
          //     .then(updateRes => {
          //       res.redirect(
          //         "/payment/rzp/fail?reason=" + r.error.description
          //       );
          //     })
          //     .catch(err => {
          //       res.redirect(
          //         "/payment/rzp/fail?reason=" + r.error.description
          //       );
          //     });
        } else {
          // redirect to success page
          console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!success transactions");
          res.redirect("/payment/rzp/success");

          //   updateTransaction(orderCode, {
          //     status: "SUCCESS",
          //     payment_gateway_order_id: razorpay_payment_id,
          //     transaction_response: resBody
          //   })
          //     .then(updateRes => {
          //       console.log("successfully payment success");
          //       res.redirect("/payment/rzp/success");
          //     })
          //     .catch(err => {
          //       res.redirect("/payment/rzp/success");
          //     });
        }
      })
      .catch((err) => {
        console.log("/razorpay/handler Error 1 => ", err);
        res.redirect("/payment/rzp/fail");

        // updateTransaction(orderCode, {
        //   status: "UNSUCCESS",
        //   payment_gateway_order_id: razorpay_payment_id,
        //   transaction_response: resBody
        // })
        //   .then(updateRes => {
        //     res.redirect("/payment/rzp/fail?reason=Not able to validate");
        //   })
        //   .catch(err => {
        //     res.redirect("/payment/rzp/fail?reason=Not able to validate");
        //   });
      });
  } catch (err) {
    console.log("/razorpay/handler Error => ", err);
    res.redirect("/payment/rzp/fail");

    // updateTransaction(orderCode, {
    //   status: "UNSUCCESS",
    //   payment_gateway_order_id: razorpay_payment_id,
    //   transaction_response: resBody
    // })
    //   .then(updateRes => {
    //     res.redirect("/payment/rzp/fail");
    //   })
    //   .catch(err => {
    //     res.redirect("/payment/rzp/fail");
    //   });
    // res.redirect("/payment/rzp/fail");
  }
});

router.get("/success", (req, res) => {
  res.send("Success");
});

router.get("/fail", (req, res) => {
  res.send("Fail");
});

// router.get('/handler', (req, res) => {
//   console.log('got get request');
// })

module.exports = router;

const createRazorpayOrder = ({ amount, orderCode }) => {
  console.log("Create", orderCode);
  const url = "https://api.razorpay.com/v1/orders";
  var options = {
    method: "POST",
    headers: {
      "cache-control": "no-cache",
      "content-type": "application/json",
      authorization: `Basic ${Buffer.from(
        `${RAZORPAY_KEY_ID}:${RAZORPAY_SECRET}`
      ).toString("base64")}`,
    },
    body: JSON.stringify({
      amount: amount,
      currency: "INR",
      receipt: orderCode,
      payment_capture: false,
      notes: {},
    }),
    json: true,
  };
  return new Promise((resolve, reject) => {
    fetch(url, options)
      .then((response) => response.json())
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

function validateTransaction(amount, payment_id) {
  return new Promise((resolve, reject) => {
    request(
      {
        method: "POST",
        url:
          "https://" +
          RAZORPAY_KEY_ID +
          ":" +
          RAZORPAY_SECRET +
          "@api.razorpay.com/v1/payments/" +
          payment_id +
          "/capture",
        form: {
          amount,
        },
      },
      (error, response, body) => {
        if (error) {
          const err = JSON.stringify(error);
          reject(err);
        }

        console.log("Response:", body);
        const newRes = JSON.parse(body);

        console.log("Response  1========>>>:", newRes);

        resolve(newRes);
      }
    );
  });
}
