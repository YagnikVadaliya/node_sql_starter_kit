import AppController from "./appcontroller";
import constants from "../configuration/constants";
const models = require("../models");

class Comman extends AppController {
  constructor() {
    super();
  }

  /* Admins functions */

  /**
   * @api {get} /v1/auth/all-kpis (all kpis for dashboard)
   * @apiName listAllKpis
   * @apiGroup Comman
   *
   *
   * @apiSuccess {Object} Comman.
   */
  async listAllKpis(req, res) {
    console.log("listAllKpis function start");

    try {
      let userCount = await models.users.count();
      let cityCount = await models.cities.count();
      let movieCount = await models.movies.count();
      let theaterCount = await models.theaters.count();

      console.log("userCount printed here", userCount);

      res.json({
        status: constants.success_code,
        message: "successfully listed All KPIs",
        data: {
          userCount: userCount,
          cityCount: cityCount,
          movieCount: movieCount,
          theaterCount: theaterCount,
        },
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /*
    ------------------------
    Application functions
  */

  /*
    -------------------------
    comman functions
  */
}

export default new Comman();
