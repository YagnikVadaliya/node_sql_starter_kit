/* eslint-disable */
import AppController from "./appcontroller";
import constants from "../configuration/constants";
import bcrypt from "bcryptjs";
import token from "../configuration/secret";
var jwt = require("jsonwebtoken");
const request = require("request");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const models = require("../models");

class User extends AppController {
  constructor() {
    super();
  }

  /* 
    --------------------------------------------------------------------------------
    Admins functions 
  */

  /**
   * @api {get} /v1/auth/list-user/:skip/:limit (List application User)
   * @apiName listUsers
   * @apiGroup AdminUser
   *
   *
   * @apiSuccess {Object} User.
   */
  async listUsers(req, res) {
    console.log("listUsers function start", req.params);
    try {
      let result = await models.users.findAndCountAll({
        where: {
          _deleted: false,
        },
        order: [["createdAt", "DESC"]],
        offset: parseInt(req.params.skip),
        limit: parseInt(req.params.limit),
      });

      res.json({
        status: constants.success_code,
        message: "successfully listed",
        data: result.rows,
        total: result.count,
      });

      return;
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {delete} /v1/auth/delete-user/:userId (Delete application User)
   * @apiName deleteUser
   * @apiGroup AdminUser
   *
   *
   * @apiSuccess {Object} User.
   */
  async deleteUser(req, res) {
    console.log("deleteUser function start", req.params);
    try {
      if (!req.params.userId) {
        res.json({
          status: constants.server_error_code,
          message: "Please provide all Data",
        });

        return;
      }

      let deleteData = await models.users.update(
        {
          _deleted: true,
        },
        {
          where: {
            id: req.params.userId,
          },
        }
      );

      res.json({
        status: constants.success_code,
        message: "Delete User Successfully",
        data: deleteData,
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/auth/filter-user/:skip/:limit (Filter application User)
   * @apiName filterUser
   * @apiGroup AdminUser
   *
   * @apiParam {String} search_text search_text.
   *
   *
   * @apiSuccess {Object} User.
   */
  async filterUser(req, res) {
    console.log("filterUser function start", req.body, req.params);
    try {
      if (req.body.search_text == "") {
        //search Text is blank

        let result = await models.users.findAndCountAll({
          where: {
            _deleted: false,
          },
          order: [["createdAt", "DESC"]],
          offset: parseInt(req.params.skip),
          limit: parseInt(req.params.limit),
        });

        res.json({
          status: constants.success_code,
          message: "successfully listed",
          data: result.rows,
          total: result.count,
        });

        return;
      }

      //with search result
      let searchResult = await models.users.findAndCountAll({
        where: {
          [Op.and]: [
            {
              _deleted: false,
            },
            {
              [Op.or]: [
                {
                  first_name: {
                    [Op.like]: `%${req.body.search_text}%`,
                  },
                },
                {
                  unique_id: {
                    [Op.like]: `%${req.body.search_text}%`,
                  },
                },
                {
                  last_name: {
                    [Op.like]: `%${req.body.search_text}%`,
                  },
                },
                {
                  email: {
                    [Op.like]: `%${req.body.search_text}%`,
                  },
                },
                {
                  phone: {
                    [Op.like]: `%${req.body.search_text}%`,
                  },
                },
              ],
            },
          ],
        },
        order: [["createdAt", "DESC"]],
        offset: parseInt(req.params.skip),
        limit: parseInt(req.params.limit),
      });

      res.json({
        status: constants.success_code,
        message: "successfully listed",
        data: searchResult.rows,
        total: searchResult.count,
      });

      return;
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/auth/change-user-status (change Active status of application User)
   * @apiName changeStatus
   * @apiGroup AdminUser
   *
   * @apiParam {String} userId userId.
   * @apiParam {String} flag flag.
   *
   * @apiSuccess {Object} User.
   */
  async changeStatus(req, res) {
    console.log("changeStatus function start", req.body);
    try {
      if (!req.body.userId) {
        res.json({
          status: constants.server_error_code,
          message: "Please provide all Data",
        });

        return;
      }

      let userObject = await models.users.update(
        {
          is_active: req.body.flag,
        },
        {
          where: {
            id: req.body.userId,
          },
        }
      );

      res.json({
        status: constants.success_code,
        message: "User status changed Successfully",
        data: userObject,
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/auth/edit-user-admin (edit User from admin panel)
   * @apiName editAdminUser
   * @apiGroup AdminUser
   *
   * @apiParam {String} userId userId.
   * @apiParam {String} first_name first_name (Optional).
   * @apiParam {String} last_name last_name (Optional).
   * @apiParam {String} username username (Optional).
   * @apiParam {String} referal_code referal_code (Optional).
   * @apiParam {String} email email (Optional).
   * @apiParam {String} phone phone (Optional).
   * @apiParam {String} dob dob (Optional).
   * @apiParam {String} gender gender (Optional).
   *
   * @apiSuccess {Object} User.
   */
  async editAdminUser(req, res) {
    console.log("editAdminUser function start");

    try {
      if (!req.body.userId) {
        throw "Please provide all Data";
      }

      let updateObject = {};

      if (req.body.first_name) {
        updateObject.first_name = req.body.first_name;
      }

      if (req.body.last_name) {
        updateObject.last_name = req.body.last_name;
      }

      if (req.body.username) {
        updateObject.username = req.body.username;
      }

      if (req.body.gender) {
        updateObject.gender = req.body.gender;
      }

      if (req.body.dob) {
        updateObject.dob = req.body.dob;
      }

      if (req.body.referal_code) {
        updateObject.referal_code = req.body.referal_code;
      }

      if (req.body.email) {
        updateObject.email = req.body.email;
      }

      if (req.body.phone) {
        updateObject.phone = req.body.phone;
      }

      console.log("!!!!!!!!!!!final updateObject printed here", updateObject);
      if (updateObject == {}) {
        res.json({
          status: constants.success_code,
          message: "Edit User Successfully",
          data: null,
        });

        return;
      }

      let editUserData = await models.users.update(updateObject, {
        where: {
          id: req.body.userId,
        },
      });

      res.json({
        status: constants.success_code,
        message: "Edit User Successfully",
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /*
    --------------------------------------------------------------------------------
    Application functions
  */

  /**
   * @api {post} /v1/register (Register User)
   * @apiName registerUser
   * @apiGroup AppUser
   *
   * @apiParam {String} first_name first_name.
   * @apiParam {String} last_name last_name.
   * @apiParam {String} email email.
   * @apiParam {String} password password.
   *
   * @apiSuccess {Object} User.
   */
  async registerUser(req, res) {
    console.log("registerUser function start");
    try {
      if (
        !req.body.first_name ||
        !req.body.email ||
        !req.body.password ||
        !req.body.last_name
      ) {
        throw "Please provide all Data";
      }

      let existUserData = await models.users.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (existUserData) {
        throw "User with this email is already registered with us";
      }

      let secret = token();
      var hashedPassword = bcrypt.hashSync(req.body.password, 8);
      var uniqueId = "_" + Math.random().toString(36).substr(2, 9);

      var referalString =
        "Ubragga" +
        req.body.first_name +
        Math.random().toString(36).substr(2, 4);
      var uniqueManageId = "PLAYER_" + Math.random().toString(36).substr(2, 4);

      let createObject = {
        id: uniqueId,
        // unique_id : uniqueManageId,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: hashedPassword,
        is_active: true,
        is_blocked: false,
        // wallet_balance : 0,
        won_count: 0,
        lost_count: 0,
        draw_count: 0,
        lapsed_count: 0,
        invited_count: 0,
        avg_response_time: 0.0,
        ubux_earned: 0.0,
        ubux_spent: 0.0,
        used_game_helpers: 0,
        avg_correct: 0,
        referal_code: referalString,
        _deleted: false,
        social_id: null,
        social_response: null,
        login_with: "normal",
      };

      if (req.body.phone) {
        createObject.phone = req.body.phone;
      }

      let user = await models.users.create(createObject);

      let updatedUserData = await models.users.findOne({
        where: {
          id: user.id,
        },
      });

      let authToken = jwt.sign({ id: user.id, role: "User" }, secret, {
        expiresIn: 86400, // expires in 24 hours
      });
      console.log("authToken printed here", authToken);

      res.json({
        status: constants.success_code,
        message: "successfully created",
        data: updatedUserData,
        authToken: authToken,
      });

      return;
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/login (Login User)
   * @apiName login
   * @apiGroup AppUser
   *
   * @apiParam {String} email email.
   * @apiParam {String} password password.
   *
   * @apiSuccess {Object} User.
   */
  async login(req, res) {
    console.log("login function start");
    try {
      if (!req.body.email || !req.body.password) {
        throw "Please provide all Data";
      }

      let user = await models.users.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (!user) {
        throw "Invalid Username";
      }

      var isEqual = bcrypt.compareSync(req.body.password, user.password);
      console.log("!!!!!!!!!!after compare", isEqual);

      if (!isEqual) {
        throw "Invalid password";
      }

      let updatedUserData = await models.users.findOne({
        where: {
          id: user.id,
        },
      });

      let secret = token();
      let authToken = jwt.sign({ id: user.id, role: "User" }, secret, {
        expiresIn: 86400, // expires in 24 hours
      });

      console.log("!!!!!!!!all process completed", updatedUserData, authToken);

      res.json({
        status: constants.success_code,
        message: "successfully Logged in",
        data: updatedUserData,
        authToken: authToken,
      });

      return;
    } catch (error) {
      console.log("!!!!!!!!error printed here", error);
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/generate-otp (Generate OTP for User)
   * @apiName generateOtp
   * @apiGroup AppUser
   *
   * @apiParam {String} phone phone (Optional).
   * @apiParam {String} email email (Optional).
   * @apiParam {Boolean} isRegister isRegister (login (false) / register (true)).
   * @apiParam {String} otpText otpText (Optional).
   *
   *
   * @apiSuccess {Object} User.
   */
  async generateOtp(req, res) {
    console.log("generateOtp function start", req.body);
    try {
      if (!req.body.isRegister) {
        console.log("!!!!!!!!!!!!!check for login condition");

        //check phone exist or not for login condition
        let userData = await models.users.findOne({
          where: {
            [Op.or]: [
              {
                phone: req.body.phone ? req.body.phone : "",
              },
              {
                email: req.body.email ? req.body.email : "",
              },
            ],
          },
        });

        if (!userData) {
          throw "No data found for this phone";
        }
      }

      let otpCode;

      if (req.body.otpText) {
        otpCode = req.body.otpText;
      } else {
        otpCode = new User().getOtp();
      }

      if (req.body.phone) {
        var mesageValue =
          "This is the generated otp for the Ubragga. And following is the otp : " +
          otpCode;
        let apiKey = "29fad242-6d68-487c-845c-725e337a0b5b";
        let url = `https://www.alcodes.com/api/sms-compose?message=${mesageValue}&phoneNumbers=${req.body.phone}&countryCode=IN&smsSenderId=AUTHTP&smsTypeId=1&is_otp=true&walletType=DOMESTIC&username=${apiKey}&password=passwd`;

        var options = {
          method: "GET",
          url: url,
          headers: {},
        };

        //send otp using alcodes
        var dataValue = await new Promise((resolve, reject) => {
          request(options, function (error, response, body) {
            if (error) {
              console.log("!!!!!!!!!!!error printed here", error);
              reject(error);
            }
            if (body && body.error) {
              resolve(body);
            }
            resolve(body);
          });
        });

        console.log("dataValue printed here", dataValue);
        let uniqueId = "_" + Math.random() * 9999;

        let createObject = {
          id: uniqueId,
          code: otpCode,
          phone: req.body.phone,
          _deleted: false,
        };
        console.log("!!!!!!!!createObject printed here", createObject);

        // //add otp to otp table
        let otpData = await models.user_otps.create(createObject);
        console.log("!!!!!!!!!otpData created succssfully", otpData);

        res.json({
          status: constants.success_code,
          message: "Otp sent to registerd phone phone",
          data: otpCode,
        });

        return;
      }

      //send email
      var mesageValue =
        "This is the generated otp for the Ubragga. And following is the otp : " +
        otpCode;
      let resMailData = await MailerService.sendMail(
        res,
        req.body.email,
        "OTP",
        "email/otpEmail",
        {
          textMessage: mesageValue,
        }
      );

      let uniqueId = "_" + Math.random() * 9999;
      let createObject = {
        id: uniqueId,
        code: otpCode,
        email: req.body.email,
        _deleted: false,
      };
      console.log("!!!!!!!!createObject printed here", createObject);

      // //add otp to otp table
      let otpData = await models.user_otps.create(createObject);
      console.log("!!!!!!!!!otpData created succssfully", otpData);

      res.json({
        status: constants.success_code,
        message: "Otp sent to registerd email",
        data: otpCode,
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {post} /v1/verify-otp (Verify OTP for User)
   * @apiName verifyOtp
   * @apiGroup AppUser
   *
   * @apiParam {String} otpCode otpCode .
   * @apiParam {String} email email (Optional).
   * @apiParam {String} phone phone (Optional).
   *
   * @apiSuccess {Object} User.
   */
  async verifyOtp(req, res) {
    console.log("verifyOtp function start");
    try {
      if (!req.body.otpCode) {
        throw "Please provide all Data";
      }

      let secret = token();

      //check otp is correct or not
      let otpData = await models.user_otps.findOne({
        where: {
          [Op.and]: [
            {
              code: req.body.otpCode,
            },
            {
              [Op.or]: [
                {
                  phone: req.body.phone,
                },
                {
                  email: req.body.email,
                },
              ],
            },
          ],
        },
      });
      console.log("!!!!!!!!otpData printed here", otpData);

      if (!otpData) {
        throw "OTP is incorrect";
      }

      console.log("!!!!!!!!!!!!!otp matched");

      //delete otp from otpTable
      let deleteOtpData = await models.user_otps.destroy({
        where: {
          [Op.or]: [
            {
              phone: req.body.phone,
            },
            {
              email: req.body.email,
            },
          ],
        },
      });
      console.log(
        "!!!!!!!!!!!!!!!!!!deleteOtpData printed here",
        deleteOtpData
      );

      res.json({
        status: constants.server_error_code,
        message: "OTP correct",
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /*
    --------------------------------------------------------------------------------
    comman functions
  */

  /*
    name: getOtp
    target: Application
    description: generate otp random String
    parameters:  null
    response: otpString 
  */
  getOtp() {
    // Declare a digits variable
    // which stores all digits
    var digits = "0123456789";
    let OTP = "";
    for (let i = 0; i < 4; i++) {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

  /**
   * @api {post} /v1/auth/edit-user (edit User from application)
   * @apiName editUser
   * @apiGroup User
   *
   * @apiParam {String} userId userId.
   * @apiParam {String} first_name first_name (Optional).
   * @apiParam {String} last_name last_name (Optional).
   * @apiParam {String} username username (Optional).
   * @apiParam {String} referal_code referal_code (Optional).
   * @apiParam {String} email email (Optional).
   * @apiParam {String} phone phone (Optional).
   * @apiParam {String} dob dob (Optional).
   * @apiParam {String} gender gender (Optional).
   *
   * @apiSuccess {Object} User.
   */
  async editUser(req, res) {
    console.log("editUser function start", req.user.id);

    try {
      let updateObjeect = {};

      if (req.body.first_name) {
        updateObject.first_name = req.body.first_name;
      }

      if (req.body.last_name) {
        updateObject.last_name = req.body.last_name;
      }

      if (req.body.username) {
        updateObject.username = req.body.username;
      }

      if (req.body.gender) {
        updateObject.gender = req.body.gender;
      }

      if (req.body.dob) {
        updateObject.dob = req.body.dob;
      }

      if (req.body.referal_code) {
        updateObject.referal_code = req.body.referal_code;
      }

      if (req.body.email) {
        updateObject.email = req.body.email;
      }

      if (req.body.phone) {
        updateObject.phone = req.body.phone;
      }

      console.log("!!!!!!!!!!!final updateObject printed here", updateObject);
      if (updateObject == {}) {
        res.json({
          status: constants.success_code,
          message: "Edit User Successfully",
          data: null,
        });

        return;
      }

      let editUserData = await models.users.update(updateObject, {
        where: {
          id: req.user.id,
        },
      });

      res.json({
        status: constants.success_code,
        message: "Edit User Successfully",
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }

  /**
   * @api {get} /v1/auth/get-login-user-detail (Get login user detail data)
   * @apiName getUserDetail
   * @apiGroup AppUser
   *
   * @apiSuccess {Object} User.
   */
  async getUserDetail(req, res) {
    console.log("getUserDetail function start", req.user.id);
    try {
      if (!req.user) {
        throw "User not found";
      }

      let userObject = await models.users.findOne({
        where: {
          id: req.user.id,
        },
      });

      res.json({
        status: constants.success_code,
        message: "User Detail Get Successfully",
        data: userObject,
      });
    } catch (error) {
      res.status(500).json({
        status: constants.server_error_code,
        message:
          typeof error === "string"
            ? error
            : typeof error.message === "string"
            ? error.message
            : constants.server_error,
      });

      return;
    }
  }
}

export default new User();
