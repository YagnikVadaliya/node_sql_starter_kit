"use strict";
module.exports = (sequelize, DataTypes) => {
  const user_otps = sequelize.define(
    "user_otps",
    {
      mobile: DataTypes.STRING,
      code: DataTypes.STRING,
      timestamp: DataTypes.DATE,
      _deleted: DataTypes.BOOLEAN,
    },
    {}
  );

  return user_otps;
};
