/* eslint-disabled */

"use strict";

require("dotenv").config();
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const basename = path.basename(__filename);
const db = {};
const dbName = process.env.DATABASE_NAME;
const dbUser = process.env.DATABASE_USER;
const dbPassword = process.env.DATABASE_PASS;

let sequelize;

//production
// sequelize = new Sequelize('income_shows', 'root', 'e593636e365a82fe', {
//   host: 'localhost',
//   dialect: 'mysql',
// });

//development
sequelize = new Sequelize(dbName, dbUser, dbPassword, {
  host: "localhost",
  dialect: "mysql",
});

const op = Sequelize.Op;
const operatorsAliases = {
  $in: op.in,
  $or: op.or,
};

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    // const model = sequelize['import'](path.join(__dirname, file));
    const model = require(`../models/${file}`)(sequelize, Sequelize.DataTypes);

    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
// db['op'] = op;

module.exports = db;
