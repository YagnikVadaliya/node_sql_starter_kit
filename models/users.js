"use strict";
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define(
    "users",
    {
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      phone: DataTypes.STRING,
      alternate_phone: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      role: DataTypes.STRING,
      profile_image: DataTypes.STRING,
      last_login_at: DataTypes.STRING,
      is_active: DataTypes.BOOLEAN,
      is_notification: DataTypes.BOOLEAN,
      is_location: DataTypes.BOOLEAN,
      city: DataTypes.STRING,
      state: DataTypes.STRING,
      country: DataTypes.STRING,
      address_line1: DataTypes.TEXT,
      address_line2: DataTypes.TEXT,
      rewards_points: DataTypes.DOUBLE,
      wallet_balance: DataTypes.DOUBLE,
      referal_code: DataTypes.STRING,
      _deleted: DataTypes.BOOLEAN,
    },
    {
      indexes: [
        {
          unique: true,
          fields: ["email"],
        },
      ],
    }
  );

  // users.prototype.toJSON =  function () {
  //   var values = Object.assign({}, this.get());
  //   delete values.password;
  //   return values;
  // };

  users.associate = function (models) {
    // users.hasOne(models.notifications, {
    //   foreignKey: 'user_id',
    //   onDelete: 'CASCASDE',
    //   onUpdate: 'CASECADE'
    // });
  };

  // await users.sync({ force: true });

  return users;
};
